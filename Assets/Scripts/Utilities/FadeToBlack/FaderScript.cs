﻿using UnityEngine;
using System.Collections;

public class FaderScript : MonoBehaviour {

	private CanvasGroup fadingCanvas;

	void Awake(){
		fadingCanvas = GetComponent<CanvasGroup> ();
	}

	/// <summary>
	/// Sets a value indicating whether this <see cref="FaderScript"/> set fading alpha.
	/// </summary>
	/// <value><c>true</c> if set fading alpha; otherwise, <c>false</c>.</value>
	public float SetFadingAlpha{
		set{fadingCanvas.alpha = value;}
	}

	/// <summary>
	/// Gets the get fading canvas.
	/// </summary>
	/// <value>The get fading canvas.</value>
	public CanvasGroup GetFadingCanvas{
		get{return fadingCanvas;}
	}

}

﻿using UnityEngine;
using System.Collections;
using LOS.Event;
using LOS;

public class RelayScript : MonoBehaviour {

	private LOSEventSource sourceEvent;
	private LOSRadialLight radialLight;
	private DialoguerManager dialoguerManager;
	private bool isDialoguerOver;
	private AudioSource audioSource;
	private bool hasPlayed = false;

	void Awake(){
		isDialoguerOver = false;
		sourceEvent = transform.parent.GetComponentInChildren<LOSEventSource> ();
		radialLight = transform.parent.GetComponentInChildren<LOSRadialLight> ();
		dialoguerManager = GameObject.FindGameObjectWithTag ("DialoguerManager").GetComponent<DialoguerManager>();
		audioSource = GetComponent<AudioSource> ();
	}

	// Use this for initialization
	void Start () {
		SetRadialLightActive = false;
	}

	/// <summary>
	/// Raises the collision enter event.
	/// </summary>
	/// <param name="other">Other.</param>
	void OnCollisionEnter( Collision other){
		if (other.gameObject.tag == "Player") {

			if (!hasPlayed) {
				audioSource.Play ();
				hasPlayed = true;
			}

			if (!Dialoguer.GetGlobalBoolean(4) && !isDialoguerOver) {
				StartCoroutine (dialoguerManager.StartDialogue (0.2f, 4, new Vector2 (204, 310), true, false));
				isDialoguerOver = true;
			}
			EnableRadialLight (other);
		}
	}

	/// <summary>
	/// Enables the radial light.
	/// </summary>
	/// <param name="other">Other.</param>
	private void EnableRadialLight(Collision other){
		if (other.gameObject.tag == "Player") {
			if (!sourceEvent.enabled) {
				sourceEvent.enabled = true;
				radialLight.enabled = true;
			}
		}
	}

	/// <summary>
	/// Sets a value indicating whether this <see cref="RelayScript"/> set radial light active.
	/// </summary>
	/// <value><c>true</c> if set radial light active; otherwise, <c>false</c>.</value>
	public bool SetRadialLightActive{
		set{sourceEvent.enabled = value; radialLight.enabled = value;}
	}
}

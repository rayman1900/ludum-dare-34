﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class AudioManager : MonoBehaviour {

	public AudioClip[] musicTracks;
	public AudioClip[] transitionTracks;

	private AudioSource audioSource;

	void Awake(){
		audioSource = GetComponent<AudioSource> ();
	}

	public AudioSource getCurrentAudioSource{
		get{ return audioSource;}
	}

	public void PlaySelectedSoundtrack(AudioClip clipToPlay, double timeToRead){
		
			audioSource.clip = clipToPlay;
			audioSource.PlayScheduled(timeToRead);

	}
}

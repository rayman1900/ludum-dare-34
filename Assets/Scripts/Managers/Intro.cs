﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Intro : MonoBehaviour {

	private GameObject legacyRpg;
	private DialoguerManager dialoguerManager;
	private LegacyClassicRpgGui classicRpgGUI;
	private AudioManager audioManager;


	void Awake(){
		legacyRpg = GameObject.FindGameObjectWithTag ("DialoguerGUI");
		classicRpgGUI = legacyRpg.GetComponent<LegacyClassicRpgGui> ();
		dialoguerManager = GameObject.Find ("DialoguerManager").GetComponent<DialoguerManager> ();
		audioManager = GameObject.FindGameObjectWithTag ("SoundManager").GetComponent<AudioManager> ();
	}

	// Use this for initialization
	void Start () {
		Dialoguer.events.onEnded += onDialogueEndedHandler;
		Dialoguer.events.onMessageEvent += onDialoguerMessageEvent;
		audioManager.PlaySelectedSoundtrack(audioManager.musicTracks[0], 0);
		StartCoroutine (dialoguerManager.StartDialogue(0.1f, 0, new Vector2(330, 160), true, true));
	}

	private void onDialogueEndedHandler(){
		
		classicRpgGUI._showDialogueBox = false;
		DontDestroyOnLoad(GameStateManager.Instance);
		GameStateManager.Instance.startState();

	}

	private void onDialoguerMessageEvent(string message, string metadata){
		if (message == "StartIntro") {
			classicRpgGUI.dialogByClick = false;
			classicRpgGUI.boxPosition = new Vector2 (204, 310);
		}

		if (message == "IntroEnded") {
			audioManager.getCurrentAudioSource.DOFade (0, 0.5f);
		}
	}

	void OnDestroy(){
		Dialoguer.events.onEnded -= onDialogueEndedHandler;
		Dialoguer.events.onMessageEvent -= onDialoguerMessageEvent;
	}

}

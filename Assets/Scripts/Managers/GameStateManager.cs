﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameStateManager : MonoBehaviour {

	// Declare properties
	private static GameStateManager instance;
	private bool isPlayerDead;
	private int truthObtained;
	private bool isTruthDiscovered;

	// ---------------------------------------------------------------------------------------------------
	// gamestate()
	// --------------------------------------------------------------------------------------------------- 
	// Creates an instance of gamestate as a gameobject if an instance does not exist
	// ---------------------------------------------------------------------------------------------------
	public static GameStateManager Instance
	{
		get
		{
			if(instance == null)
			{
				instance = new GameObject("GameStateManager").AddComponent<GameStateManager>();
			}

			return instance;
		}
	}	

	// Sets the instance to null when the application quits
	public void OnApplicationQuit()
	{
		instance = null;
	}
	// ---------------------------------------------------------------------------------------------------


	// ---------------------------------------------------------------------------------------------------
	// startState()
	// --------------------------------------------------------------------------------------------------- 
	// Creates a new game state
	// ---------------------------------------------------------------------------------------------------
	public void startState()
	{
		print ("Creating a new game state");

		// Set default properties:
		isPlayerDead = false;
		truthObtained = 0;
		isTruthDiscovered = false;

		// Load level 1
		SceneManager.LoadScene ("Level0");
	}

	// ---------------------------------------------------------------------------------------------------
	// getLevel()
	// --------------------------------------------------------------------------------------------------- 
	// Returns the currently active level
	// ---------------------------------------------------------------------------------------------------
	public string getLevel()
	{
		return SceneManager.GetActiveScene().name;
	}


	// ---------------------------------------------------------------------------------------------------
	// setLevel()
	// --------------------------------------------------------------------------------------------------- 
	// Sets the currently active level to a new value
	// ---------------------------------------------------------------------------------------------------
	public void setLevel(string newLevel)
	{
		// Set activeLevel to newLevel
		SceneManager.LoadScene(newLevel);
	}

	// ---------------------------------------------------------------------------------------------------
	// getIsPlayerDead()
	// --------------------------------------------------------------------------------------------------- 
	// Returns the player is dead
	// ---------------------------------------------------------------------------------------------------
	public bool getIsPlayerDead
	{
		get{return isPlayerDead;}
	}


	// ---------------------------------------------------------------------------------------------------
	// setIfPlayerDead()
	// --------------------------------------------------------------------------------------------------- 
	// Sets if the player is dead
	// ---------------------------------------------------------------------------------------------------
	public void setIfPlayerDead(bool isDead)
	{
		isPlayerDead = isDead;
	}


	// ---------------------------------------------------------------------------------------------------
	// getTruthNumber
	// --------------------------------------------------------------------------------------------------- 
	// Returns how much truth the player discovered
	// ---------------------------------------------------------------------------------------------------
	public int getTruthNumber
	{
		get{return truthObtained;}
	}


	// ---------------------------------------------------------------------------------------------------
	// AddTruth
	// --------------------------------------------------------------------------------------------------- 
	// Add a truth to total truth number
	// ---------------------------------------------------------------------------------------------------
	public void AddTruth()
	{
		truthObtained++;
	}

	// ---------------------------------------------------------------------------------------------------
	// getIsTruthDiscovered
	// --------------------------------------------------------------------------------------------------- 
	// Returns the truth is discovered
	// ---------------------------------------------------------------------------------------------------
	public bool getIsTruthDiscovered
	{
		get{return isTruthDiscovered;}
	}


	// ---------------------------------------------------------------------------------------------------
	// setIsTruthDiscovered()
	// --------------------------------------------------------------------------------------------------- 
	// Sets if the truth is discovered
	// ---------------------------------------------------------------------------------------------------
	public void setIsTruthDiscovered(bool isDiscovered)
	{
		isTruthDiscovered = isDiscovered;
	}
}

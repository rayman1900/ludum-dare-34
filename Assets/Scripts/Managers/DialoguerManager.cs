﻿using UnityEngine;
using System.Collections;

public class DialoguerManager : MonoBehaviour {
	
	private GameObject classicRPGDialogue;
	private LegacyClassicRpgGui classicGUI;

	void Awake(){
		classicRPGDialogue = GameObject.FindGameObjectWithTag ("DialoguerGUI");
		classicGUI = classicRPGDialogue.GetComponent<LegacyClassicRpgGui> ();
	}

	/// <summary>
	/// Starts the dialogue.
	/// </summary>
	/// <returns>The dialogue.</returns>
	/// <param name="dialogNum">Dialog number.</param>
	public IEnumerator StartDialogue(float secToStart, int dialogNum, Vector2 dialogPosition, bool isShowingDialogueBox, bool isDialogByClick){

		yield return new WaitForSeconds (secToStart);

		if (classicRPGDialogue != null) {
			classicGUI.boxPosition = dialogPosition;
			classicGUI._showDialogueBox = isShowingDialogueBox;
			classicGUI.dialogByClick = isDialogByClick;
		}

		Dialoguer.StartDialogue (dialogNum);
	}
}

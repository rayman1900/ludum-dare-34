﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class CinematicManager : MonoBehaviour {

	public Transform targetToMoveCamTo;

	private CameraManager camManager;

	void Awake(){
		camManager = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<CameraManager> ();
	}

	// Use this for initialization
	void Start () {
		Dialoguer.events.onMessageEvent += onDialoguerMessageEvent;
	}

	private void onDialoguerMessageEvent(string message, string metadata){
		if (message == "StartMovingCam" && metadata == SceneManager.GetActiveScene().name) {

			switch (SceneManager.GetActiveScene().name) {

			case "Level0":
				camManager.ShowPointOfInterest (Camera.main.transform.position, new Vector3(targetToMoveCamTo.position.x, targetToMoveCamTo.position.y, Camera.main.transform.position.z), 2.0f, Ease.Linear);
				break;

			default:
				Debug.LogError ("Did not find any camera!");
				break;
			}
		}
	}


	void OnDestroy(){
		Dialoguer.events.onMessageEvent -= onDialoguerMessageEvent;
	}
}

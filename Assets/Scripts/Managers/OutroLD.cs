﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class OutroLD : MonoBehaviour {

	private GameObject legacyRpg;
	private DialoguerManager dialoguerManager;
	private LegacyClassicRpgGui classicRpgGUI;
	private AudioManager audioManager;


	void Awake(){
		legacyRpg = GameObject.FindGameObjectWithTag ("DialoguerGUI");
		classicRpgGUI = legacyRpg.GetComponent<LegacyClassicRpgGui> ();
		dialoguerManager = GameObject.Find ("DialoguerManager").GetComponent<DialoguerManager> ();
		audioManager = GameObject.FindGameObjectWithTag ("SoundManager").GetComponent<AudioManager> ();
	}

	// Use this for initialization
	void Start () {
		Dialoguer.events.onEnded += onDialogueEndedHandler;
		audioManager.PlaySelectedSoundtrack(audioManager.musicTracks[0], 0);
		StartCoroutine (dialoguerManager.StartDialogue(0.1f, 6, new Vector2(204, 310), true, false));
	}

	private void onDialogueEndedHandler(){
		audioManager.getCurrentAudioSource.DOFade (0, 0.5f);
		classicRpgGUI._showDialogueBox = false;

	}
		
	void OnDestroy(){
		Dialoguer.events.onEnded -= onDialogueEndedHandler;
	}

}

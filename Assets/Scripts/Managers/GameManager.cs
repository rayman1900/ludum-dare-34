﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class GameManager : MonoBehaviour {

	private DialoguerManager dialoguerManager;
	private string currentLevel;
	private GameObject classicRPGDialogue;
	private GameObject player;
	private MovePlayer playerMoveScript;
	private Survival survivalScript;
	private GameObject fadingObject;
	private CanvasGroup fadingCanvas;
	private AudioManager audioManager;

	void Awake(){
		dialoguerManager = GameObject.FindGameObjectWithTag ("DialoguerManager").GetComponent<DialoguerManager>();
		currentLevel = GameStateManager.Instance.getLevel();
		player = GameObject.FindGameObjectWithTag("Player");
		playerMoveScript = player.GetComponent<MovePlayer> ();
		survivalScript = player.GetComponent<Survival> ();
		fadingObject = GameObject.FindGameObjectWithTag ("Fader");
		fadingCanvas = fadingObject.GetComponent<CanvasGroup> ();
		audioManager = GameObject.FindGameObjectWithTag ("SoundManager").GetComponent<AudioManager> ();
	}

	void Start(){

		GameLib.EventManager.RegisterListener("Death_State", this.OnDeathStateUpdate);
		Dialoguer.events.onEnded += onDialogueEndedHandler;

		switch (currentLevel) {
		case "Level0":

			//Start music

			audioManager.PlaySelectedSoundtrack(audioManager.musicTracks[0], 0);
			audioManager.getCurrentAudioSource.DOFade (1, 2f);

			fadingCanvas.alpha = 1;
			fadingCanvas.DOFade (0, 1);
			if (!Dialoguer.GetGlobalBoolean (2)) {
				playerMoveScript.isMoving = false;
				StartCoroutine (dialoguerManager.StartDialogue (1f, 2, new Vector2 (204, 310), true, false));
			}

			if(Dialoguer.GetGlobalBoolean(2)){
				playerMoveScript.isMoving = true;
			}
			break;

		default:
			Debug.LogError ("Current scene not detected!");
			break;
		}
	}

	private void OnDeathStateUpdate(object sender, object args){
		if (GameStateManager.Instance.getIsPlayerDead) {
			fadingCanvas.DOFade (1, 1);
			StartCoroutine (dialoguerManager.StartDialogue (1f, 1, new Vector2 (265, 160), true, false));
		}
	}

	private void onDialogueEndedHandler(){
		if (GameStateManager.Instance.getIsPlayerDead) {
			GameObject legacyRpg = GameObject.FindGameObjectWithTag ("DialoguerGUI");
			LegacyClassicRpgGui classicRpgGUI = legacyRpg.GetComponent<LegacyClassicRpgGui> ();
			classicRpgGUI._showDialogueBox = false;
			ResetPlayer ();
		}
	}

	void OnDestroy(){
		GameLib.EventManager.RemoveListener("Death_State", this.OnDeathStateUpdate);
		Dialoguer.events.onEnded -= onDialogueEndedHandler;
	}

	/// <summary>
	/// Resets the player.
	/// </summary>
	public void ResetPlayer(){
		GameStateManager.Instance.setIfPlayerDead (false);
		GameStateManager.Instance.setLevel (GameStateManager.Instance.getLevel ());
		playerMoveScript.isMoving = true;
	}

}

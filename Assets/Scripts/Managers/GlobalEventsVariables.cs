﻿using UnityEngine;
using System.Collections;

public class GlobalEventsVariables
{
	public bool isCurrentExitDoorOpened = false;
	public bool isSecretRoomOpened = false;
	public int truthCollected = 0;
	public string gameState = string.Empty;
	public string currentLevel = string.Empty;
	public bool isFinishedFading = false;
	public bool isPlayerDead = false;
}

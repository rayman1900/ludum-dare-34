using UnityEngine;
using System.Collections;
using DG.Tweening;
using LOS;

public class CameraManager : MonoBehaviour
{
	Follow followScript;
	LOSCamera losCam;

	void Awake(){
		DOTween.Init(true, true, LogBehaviour.Default);

		followScript = GetComponent<Follow> ();
		losCam = GetComponent<LOSCamera> ();
	}

	void Start(){
	}
	
	/// <summary>
	/// Shows the point of interest.
	/// </summary>
	/// <param name="targetLocation">Target location.</param>
	/// <param name="interval">Interval.</param>
	/// <param name="easeType">Ease type.</param>
	public void ShowPointOfInterest(Vector3 initialPosition, Vector3 targetLocation, float interval, Ease easeType){
		Sequence showingPointOfInterest_Sequence;
		showingPointOfInterest_Sequence = DOTween.Sequence ();
		//Cam follow script
		followScript.isCutscene = true;

		losCam.enabled = false;

		//Let the tween start after a while
		showingPointOfInterest_Sequence.PrependInterval (interval);
		//Moving the cam to the desired target location
		showingPointOfInterest_Sequence.Append (transform.DOMove (targetLocation, 1.0f, false).SetEase(easeType));
		//Waiting an interval before going back to inital location
		showingPointOfInterest_Sequence.AppendInterval (interval);
		//Going back to initial location
		showingPointOfInterest_Sequence.Append(transform.DOMove (initialPosition, 0.5f, false).SetEase(easeType));

		showingPointOfInterest_Sequence.OnComplete (()=>OnCutsceneComplete(followScript));
	}

	private void OnCutsceneComplete(Follow followScript){
		MovePlayer movePlayer = GameObject.FindGameObjectWithTag ("Player").GetComponent<MovePlayer> ();
		movePlayer.isMoving = true;
		followScript.isCutscene = false;
		//Raise the endGame event
		GlobalEventsVariables globals = new GlobalEventsVariables {
			gameState = "InLevel"
		};

		GameLib.EventManager.RaiseEvent("gameState_status", gameObject, globals);
	}

	/// <summary>
	/// Shakes the cam.
	/// </summary>
	/// <param name="duration">Duration.</param>
	/// <param name="strength">Strength.</param>
	/// <param name="vibrato">Vibrato.</param>
	/// <param name="randomness">Randomness.</param>
	/// <param name="ignoreZAxis">If set to <c>true</c> ignore Z axis.</param>
	public void ShakeCam(float duration, float strength, int vibrato, float randomness, bool ignoreZAxis){
		followScript.isCutscene = true;
		MovePlayer movePlayer = GameObject.FindGameObjectWithTag ("Player").GetComponent<MovePlayer> ();
		movePlayer.isMoving = false;
		DOTween.Shake(()=> transform.position, x=> transform.position = x, duration, strength, vibrato, randomness, ignoreZAxis).OnComplete(OnShakeComplete);
	}

	private void OnShakeComplete(){
		MovePlayer movePlayer = GameObject.FindGameObjectWithTag ("Player").GetComponent<MovePlayer> ();
		movePlayer.isMoving = true;
		followScript.isCutscene = false;
	}

}


﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class SwitchScript : MonoBehaviour {

	Animator switchAnim;
	private DialoguerManager dialoguerManager;
	private CameraManager camManager;

	private AudioSource audioSource;

	void Awake(){
		camManager = Camera.main.GetComponent<CameraManager> ();
		dialoguerManager = GameObject.FindGameObjectWithTag ("DialoguerManager").GetComponent<DialoguerManager>();
		audioSource = GetComponent<AudioSource> ();
	}

	// Use this for initialization
	void Start () {
		switchAnim = GetComponent<Animator> ();
	}


	/// <summary>
	/// Raises the collision enter event.
	/// </summary>
	/// <param name="other">Other.</param>
	void OnCollisionEnter( Collision other){
		if (other.gameObject.tag == "Player") {
			switchAnim.SetBool ("hasBeenPushed", true);

			camManager.ShakeCam (2.5f, 0.1f, 5, 0.1f, true);

			StartCoroutine (dialoguerManager.StartDialogue (1f, 5, new Vector2 (204, 310), true, false));

			audioSource.Play ();

			GlobalEventsVariables globals = new GlobalEventsVariables {
				isSecretRoomOpened = true
			};

			GameLib.EventManager.RaiseEvent("secret_room_openState", gameObject, globals);
		}
	}
		
}

﻿using UnityEngine;
using System.Collections;
using LOS.Event;
using LOS;

public class SecretLightState : MonoBehaviour {

	protected LOSEventSource sourceEvent;
	protected LOSRadialLight radialLight;
	protected bool secretRoomOpened = false;

	// Use this for initialization
	protected virtual void Start () {
		sourceEvent = GetComponent<LOSEventSource> ();
		radialLight = GetComponent<LOSRadialLight> ();

		GameLib.EventManager.RegisterListener("secret_room_openState", this.OnSecretRoomOpenState);

		SetRadialLightActive = false;
	}

	private void OnSecretRoomOpenState(object sender, object args){
		GlobalEventsVariables globals = args as GlobalEventsVariables;
		GameObject theSender = sender as GameObject;

		if (globals.isSecretRoomOpened) {
			EnableRadialLight ();
			secretRoomOpened = globals.isSecretRoomOpened;
		}
	}

	/// <summary>
	/// Enables the radial light.
	/// </summary>
	/// <param name="other">Other.</param>
	private void EnableRadialLight(){
		if (!sourceEvent.enabled) {	
			radialLight.enabled = true;
			sourceEvent.enabled = true;
		}
	}

	/// <summary>
	/// Sets a value indicating whether this <see cref="RelayScript"/> set radial light active.
	/// </summary>
	/// <value><c>true</c> if set radial light active; otherwise, <c>false</c>.</value>
	public bool SetRadialLightActive{
		set{sourceEvent.enabled = value; radialLight.enabled = value;}
	}

	void OnDestroy(){
		GameLib.EventManager.RemoveListener("secret_room_openState", this.OnSecretRoomOpenState);
	}
}

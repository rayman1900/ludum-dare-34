﻿using UnityEngine;
using System.Collections;

public class BlinkingLight : SecretLightState
{
	private bool isLit = false;

	public int rateOfBlinking;

	protected override void Start(){
		base.Start ();

		InvokeRepeating ("Blink", rateOfBlinking, rateOfBlinking);
	}

	private void Blink(){
		isLit = !isLit;
	}

	void Update(){

		if (secretRoomOpened) {
			if (isLit) {
				radialLight.enabled = true;
				sourceEvent.enabled = true;
			} else{
				radialLight.enabled = false;
				sourceEvent.enabled = false;
			}	
		}
	}


}




﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using LOS;

public class AltarScript : MonoBehaviour {

	private DialoguerManager dialoguerManager;
	private CameraManager camManager;
	private Rotate rotateScript;
	private LOSRadialLight radialLight;

	private Follow followScript;
	private MovePlayer movePlayer;
	private Survival survivalScript;
	private AudioManager audioManager;


	void Awake(){
		dialoguerManager = GameObject.FindGameObjectWithTag ("DialoguerManager").GetComponent<DialoguerManager>();
		camManager = Camera.main.GetComponent<CameraManager> ();
		rotateScript = GetComponent<Rotate> ();
		radialLight = transform.parent.GetChild(0).GetComponent<LOSRadialLight> ();
		followScript = Camera.main.GetComponent<Follow> ();
		movePlayer = GameObject.FindGameObjectWithTag ("Player").GetComponent<MovePlayer> ();
		survivalScript = GameObject.FindGameObjectWithTag ("Player").GetComponent<Survival> ();
		audioManager = GameObject.FindGameObjectWithTag ("SoundManager").GetComponent<AudioManager> ();
	}

	// Use this for initialization
	void Start () {
		Dialoguer.events.onEnded += onDialogueEndedHandler;

		if (Dialoguer.GetGlobalBoolean (3)) {
			transform.localScale = new Vector3 (0, 0, 0);
			radialLight.radius = 0;
		}
	}

	void Update(){
		if (Input.GetKeyUp(KeyCode.K)) {
			print (GameStateManager.Instance.getTruthNumber);
		}
	}

	void OnTriggerEnter( Collider other){
		if (other.gameObject.tag == "Player" && !GameStateManager.Instance.getIsTruthDiscovered) {
			//For that level, do Altar tranformation
			if (GameStateManager.Instance.getLevel () == "Level0" && !Dialoguer.GetGlobalBoolean (3)) {
				//Fade out current audio track
				audioManager.getCurrentAudioSource.DOFade (0, 0.5f);

				//Altar anim transform
				AltarTransformation (1, 6f);	
			}
		}
	}


	public void AltarTransformation(float size, float duration){
		movePlayer.isMoving = false;

		Sequence AltarSequence;
		AltarSequence = DOTween.Sequence ();

		AltarSequence.Append (Camera.main.DOOrthoSize (size, duration));
		AltarSequence.Insert(0, DOTween.To(x => rotateScript.speed = x, 10, 1000, duration));
		AltarSequence.Insert (0, DOTween.To (x => radialLight.radius = x, 0.8f, 4, duration));
		AltarSequence.Insert (0, audioManager.getCurrentAudioSource.DOFade (0, 0.5f).OnComplete(OnAudioTransitionIn));
		AltarSequence.AppendInterval (1f);
		AltarSequence.Append (Camera.main.DOOrthoSize (2, duration).OnComplete(OnCompleteZoom));
		AltarSequence.Insert (13, audioManager.getCurrentAudioSource.DOFade (0, 0.5f).OnComplete(OnAudioTransitionOut));
		AltarSequence.Insert(duration, DOTween.To(x => rotateScript.speed = x, 1000, 0, duration));
		AltarSequence.Insert (duration, DOTween.To (x => radialLight.radius = x, 4, 0, duration));
		AltarSequence.Insert (duration, transform.DOScale (new Vector3 (0f, 0f, 0f), duration));
		AltarSequence.Insert (duration, DOTween.To (x => survivalScript.maxVitality = x, survivalScript.maxVitality, survivalScript.maxVitality + 20, duration));
	}

	private void OnAudioTransitionIn(){
		audioManager.getCurrentAudioSource.DOFade (1, 0.5f);
		audioManager.PlaySelectedSoundtrack (audioManager.musicTracks[2], 5f);
	}

	private void OnAudioTransitionOut(){
		audioManager.getCurrentAudioSource.DOFade (1, 0.5f);
		audioManager.PlaySelectedSoundtrack (audioManager.musicTracks[1], 0);
	}

	public void OnCompleteZoom(){
		movePlayer.isMoving = true;
		SphereCollider collider = GetComponent<SphereCollider> ();
		collider.enabled = false;
		//print ("play dialogue from truth");
		if (GameStateManager.Instance.getLevel() == "Level0" && !Dialoguer.GetGlobalBoolean(3)) {
			StartCoroutine (dialoguerManager.StartDialogue (0.5f, 3, new Vector2 (204, 310), true, false));	
		}
	}

	private void onDialogueEndedHandler(){
		if (GameStateManager.Instance.getLevel () == "Level0" && Dialoguer.GetGlobalBoolean (3) && !GameStateManager.Instance.getIsTruthDiscovered) {
			GameStateManager.Instance.AddTruth ();
			GameStateManager.Instance.setIsTruthDiscovered (true);

			audioManager.getCurrentAudioSource.DOFade (0, 0.5f).OnComplete (OnReturnToMainSoundtrack);
		}
	}

	private void OnReturnToMainSoundtrack(){
		audioManager.getCurrentAudioSource.DOFade (1, 0.5f);
		audioManager.PlaySelectedSoundtrack (audioManager.musicTracks[0], 0);
	}

	void OnDestroy(){
		Dialoguer.events.onEnded -= onDialogueEndedHandler;
	}
}

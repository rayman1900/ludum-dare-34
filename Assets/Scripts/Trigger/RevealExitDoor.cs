﻿using UnityEngine;
using System.Collections;

public class RevealExitDoor : MonoBehaviour {

	private AudioSource audioSource;

	private bool hasPlayed = false;

	void Awake(){
		audioSource = GetComponent<AudioSource> ();
	}

	void OnTriggerEnter(Collider other){
		if (other.tag == "Player") {

			if (!hasPlayed) {
				audioSource.Play ();
				hasPlayed = true;
			}


			GlobalEventsVariables globals = new GlobalEventsVariables {
				isCurrentExitDoorOpened = true
			};

			GameLib.EventManager.RaiseEvent("exit_door_openState", gameObject, globals);
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using LOS.Event;
using DG.Tweening;

public class HideOnLOSEvent : MonoBehaviour {

	public bool isHidden = false;

	private SpriteRenderer s_renderer;
	private SpriteRenderer sub_renderer;


	void Start () {

		s_renderer = GetComponent<SpriteRenderer> ();
		sub_renderer = transform.GetChild (0).GetComponent<SpriteRenderer> ();

		LOSEventTrigger trigger = GetComponent<LOSEventTrigger>();
		trigger.OnNotTriggered += OnNotLit;
		trigger.OnTriggered += OnLit;

		OnNotLit();
	}

	private void OnNotLit () {
		s_renderer.enabled = false;
		sub_renderer.enabled = false;
	}

	private void OnLit () {
		s_renderer.enabled = true;
		sub_renderer.enabled = true;

		OnFadeInFadeOut ();
	}

	private void OnFadeInFadeOut(){
		sub_renderer.DOFade (0, 1.5f).SetLoops (-1, LoopType.Yoyo);
	}

}

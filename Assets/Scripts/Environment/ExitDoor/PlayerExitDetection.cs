﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class PlayerExitDetection : MonoBehaviour {

	private GameObject fadingObject;
	private CanvasGroup fadingCanvas;
	private AudioManager audioManager;

	void Awake(){
		fadingObject = GameObject.FindGameObjectWithTag ("Fader");
		fadingCanvas = fadingObject.GetComponent<CanvasGroup> ();
		audioManager = GameObject.FindGameObjectWithTag ("SoundManager").GetComponent<AudioManager> ();
	}

	void OnTriggerEnter(Collider other){
		if (other.tag == "Player") {
			//print ("End level");
			//Stop the player
			other.GetComponent<MovePlayer> ().isPlayerMoving = false;

			fadingCanvas.DOFade (1, 1).OnComplete(OnFadeOutCompleted);
			audioManager.getCurrentAudioSource.DOFade (1, 0.5f);
		}
	}

	private void OnFadeOutCompleted(){
		GameStateManager.Instance.setLevel ("OutroLD");
	}
}

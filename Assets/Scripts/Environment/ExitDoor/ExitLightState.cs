﻿using UnityEngine;
using System.Collections;
using LOS;

public class ExitLightState : MonoBehaviour {

	private LOSRadialLight radialLight;

	// Use this for initialization
	void Start () {
		radialLight = GetComponent<LOSRadialLight> ();

		GameLib.EventManager.RegisterListener("exit_door_openState", this.OnDoorOpenState);

		radialLight.enabled = false;
	}

	private void OnDoorOpenState(object sender, object args){
		GlobalEventsVariables globals = args as GlobalEventsVariables;
		GameObject theSender = sender as GameObject;

		if (theSender as GameObject != null && globals.isCurrentExitDoorOpened) {
			radialLight.enabled = true;
		}
	}

	void OnDestroy(){
		GameLib.EventManager.RemoveListener("exit_door_openState", this.OnDoorOpenState);
	}
}

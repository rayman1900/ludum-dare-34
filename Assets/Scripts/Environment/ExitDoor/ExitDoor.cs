﻿using UnityEngine;
using System.Collections;

public class ExitDoor : MonoBehaviour {

	Animator doorAnim;

	// Use this for initialization
	void Start () {
		doorAnim = GetComponent<Animator> ();

		GameLib.EventManager.RegisterListener("exit_door_openState", this.OnDoorOpenState);
	}

	/// <summary>
	/// Raises the door open state event.
	/// </summary>
	/// <param name="sender">Sender.</param>
	/// <param name="args">Arguments.</param>
	private void OnDoorOpenState(object sender, object args){
		GlobalEventsVariables globals = args as GlobalEventsVariables;
		GameObject theSender = sender as GameObject;

		if (theSender as GameObject != null && globals.isCurrentExitDoorOpened) {
			doorAnim.SetBool ("isOpen", true);
		}
	}

	void OnDestroy(){
		GameLib.EventManager.RemoveListener("exit_door_openState", this.OnDoorOpenState);
	}
}

﻿using UnityEngine;
using System.Collections;
using LOS.Event;

public class Survival : MonoBehaviour {

	private LOSEventTrigger trigger;
	private MovePlayer movePlayerScript;
	private ParticleSystem p_system;
	private AudioSource audioSource;

	public float vitality = 100.0f;
	public float maxVitality;
	public float speed = 1.0f;

	public bool isInLight = false;

	void Awake(){
		trigger = GetComponent<LOSEventTrigger>();
		movePlayerScript = GetComponent<MovePlayer> ();
		p_system = transform.GetChild (1).GetComponent<ParticleSystem> ();
		audioSource = GetComponent<AudioSource> ();
	}

	// Use this for initialization
	void Start () {
		trigger.OnNotTriggered += OnNotLit;
		trigger.OnTriggered += OnLit;
		p_system.playbackSpeed = 2.0f;
		p_system.enableEmission = false;
		audioSource.Stop ();
	}

	void Update(){

		StartCoroutine ("TransformVitality");

		CheckDeathState ();
	}

	/// <summary>
	/// Transforms the vitality according if in light or not.
	/// </summary>
	/// <returns>The vitality.</returns>
	IEnumerator TransformVitality(){
		yield return new WaitForSeconds (0.5f);

		if (isInLight && ConvertVitality() <= maxVitality / 100.0f) {
			RegainVitality();
		} else if (!isInLight && ConvertVitality() >= 0) {
			LoseVitality();
		}

		transform.localScale = new Vector3 (ConvertVitality(), ConvertVitality(),0f);
	}
	
	private void OnNotLit(){
		isInLight = false;
		p_system.enableEmission = true;
		audioSource.Play ();
	}

	private void OnLit(){
		isInLight = true;
		p_system.enableEmission = false;
		audioSource.Stop ();
	}

	private void LoseVitality(){
		vitality -= Time.deltaTime * speed;
	}

	private void RegainVitality(){
		vitality += Time.deltaTime * speed;
	}

	public float ConvertVitality(){
		float convertedValue = vitality / 100.0f;
		Mathf.Clamp (convertedValue, 0.0f, vitality);
		return convertedValue;
	}

	private void AddMaxVitality(float bonusVit){
		float vitalityBonus = vitality + bonusVit;
		maxVitality = vitalityBonus;
		vitality = maxVitality;
	}


	private void CheckDeathState(){
		if (vitality < 10 && !GameStateManager.Instance.getIsPlayerDead) {

			movePlayerScript.isMoving = false;

			GameStateManager.Instance.setIfPlayerDead (true);

			GlobalEventsVariables globals = new GlobalEventsVariables {};
				
			GameLib.EventManager.RaiseEvent("Death_State", gameObject, globals);
		}
	}

	void OnDestroy(){
		trigger.OnNotTriggered -= OnNotLit;
		trigger.OnTriggered -= OnLit;
	}
}

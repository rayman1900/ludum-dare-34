﻿using UnityEngine;
using System.Collections;

public class MovePlayer : MonoBehaviour {

	private Rigidbody rigidBody;
	private float h;
	private float v;
	public float speed = 10f;
	public bool isMoving = false;
	public Vector3 initialPlayerPos;

	void Awake(){
		rigidBody = GetComponent<Rigidbody>();
	}

	void Start(){
		recordPlayerPosition = new Vector3 (transform.position.x, transform.position.y, transform.position.z);
		isPlayerMoving = false;
	}

	// Update is called once per frame
	void FixedUpdate () {
		
		h = Input.GetAxis ("Horizontal");
		v = Input.GetAxis ("Vertical");
		
		if (isPlayerMoving) {
			Move (rigidBody, speed, h, v);	
		}
	}

	/// <summary>
	/// Move the specified rb2D, speed, xAxis and yAxis.
	/// </summary>
	/// <param name="rb2D">Rb2 d.</param>
	/// <param name="speed">Speed.</param>
	/// <param name="xAxis">X axis.</param>
	/// <param name="yAxis">Y axis.</param>
	private void Move(Rigidbody rb, float speed, float xAxis, float yAxis)
	{
		//Managing the horizontal movement
		if (Mathf.Abs(xAxis) > 0) {
			rb.AddForce(transform.right * speed * xAxis);
		}

		if (Mathf.Abs(yAxis) > 0) {
			rb.AddForce(transform.up * speed * yAxis);
		}
	}

	public bool isPlayerMoving {
		get{return isMoving;}
		set{isMoving = value;}
	}

	public Vector3 recordPlayerPosition{
		get{return initialPlayerPos;}
		set{initialPlayerPos = value;}
	}
}

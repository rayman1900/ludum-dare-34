﻿/*
 * // the script that is added as a behaviour to the gate (for example) has one of these, and when the gate is opened, 
	// it calls "someOtherObject.RaiseGateOpened(gameobject)" or something like that.
	// Or it has it's own RaiseWhatever() function, and does the raising itself, in a similar fashion as illustrated here.
 * 
 */

using UnityEngine;
using System.Collections;

public class Gate : MonoBehaviour {

	Animator gateAnim;

	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Awake(){
		gateAnim = GetComponent<Animator> ();
		GameLib.EventManager.RegisterListener("mainscene.gate-OpenGate", this.OnGateReceiveToOpen);
	}

	//----------EVENTS-------------//

	/// <summary>
	/// Raises the gate receive to open event.
	/// </summary>
	/// <param name="sender">Sender.</param>
	/// <param name="args">Arguments.</param>
	private void OnGateReceiveToOpen(object sender, object args){
		SomeArgs theArgs = args as SomeArgs; //We just need the state of the gate as boolean
		
		RaiseGateOpened (theArgs.SomeBool); //Raise the gate and send another event to SomeManager
	}
	
	/// <summary>
	/// Raises the destroy event.
	/// </summary>
	void OnDestroy(){
		GameLib.EventManager.RemoveListener("mainscene.gate-OpenGate", this.OnGateReceiveToOpen);
	}


	//-------------------------//

	//-------FUNCTIONS---------//

	/// <summary>
	/// Raises the gate. Raise also the event to all listeners around
	/// </summary>
	/// <param name="theGate">The gate.</param>
	/// <param name="gateState">If set to <c>true</c> gate state.</param>
	public void RaiseGateOpened(bool gateState)
	{
		SomeArgs theArgs = new SomeArgs {
			SomeIntParameter = 1, //test
			SomeStringParameter = "Hello World" //test
			// or maybe something like theGate.GetComponent<MyGateScript>().someIntParameter,
			// potentially set the other arguments as well
		};

		gateAnim.SetBool ("isOpen", gateState); //Anim the gate to open or close

		// Note that this object can raise this event without knowing anything about any of the listening classes, 
		// and the listening classes don't need to know anything about this object.
		// The parameters can be anything, but Listener that care about the parameters will need to know about the parameter types.
		GameLib.EventManager.RaiseEvent("mainscene.gate-opened", gameObject, theArgs);	//Send this event to SomeManager
	}
}

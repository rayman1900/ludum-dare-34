﻿using UnityEngine;
using System.Collections;

public class Lever : MonoBehaviour {
	
	private bool isLeverActive = false;
	private bool isPlayerInBound = false;

	/// <summary>
	/// Update this instance.
	/// </summary>
	void Update(){
		if (isPlayerInBound) { //Check if we are in bounds of the lever area
			if (Input.GetKeyUp(KeyCode.E)) { //If yes, then the player can press the E key to interact
				isLeverActive = !isLeverActive; //Toggle between the raise up/raise down state
				
				SomeArgs theArgs = new SomeArgs {
					SomeBool = isLeverActive
					// or maybe something like theGate.GetComponent<MyGateScript>().someIntParameter,
					// potentially set the other arguments as well
				};
				
				GameLib.EventManager.RaiseEvent("mainscene.gate-OpenGate", gameObject, theArgs); //Raise the event mainscene.gate-OpenGate
			}
		}
	}

	//---------------EVENTS---------------//

	/// <summary>
	/// Raises the trigger stay2 d event.
	/// </summary>
	/// <param name="other">Other.</param>
	void OnTriggerStay2D(Collider2D other){
		if (other.tag == "Player") {
			isPlayerInBound = true; //We're telling we're inbound of the lever area
			//Show the interaction button
			other.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = true;
		}
	}

	/// <summary>
	/// Raises the trigger exit2 d event.
	/// </summary>
	/// <param name="other">Other.</param>
	void OnTriggerExit2D(Collider2D other){
		if (other.tag == "Player") {
			isPlayerInBound = false; //We're telling that we are outside of the lever area
			//Hide the interaction button
			other.transform.GetChild(0).GetComponent<SpriteRenderer>().enabled = false;
		}
	}

	//----------------FUNCTIONS---------//		
}

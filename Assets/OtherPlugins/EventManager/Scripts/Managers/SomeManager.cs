﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SomeManager : MonoBehaviour {

	public GameObject canvas;

	// Use this for initialization
	void Start () {
		GameLib.EventManager.RegisterListener("mainscene.gate-opened", this.OnGateOpened);
	}

	//-----------EVENTS------------//
	// do something when the "mainscene.gate-opened" event is raised.
	public void OnGateOpened(object sender, object args) {
		SomeArgs theArgs = args as SomeArgs;
		GameObject theSender = sender as GameObject;

		// (check for nulls incase the sender or the args aren't the ones you intend to be reacting to)
		// do something
		if (!canvas.transform.GetChild(2).GetComponent<Text> ().enabled) {
			canvas.transform.GetChild(2).GetComponent<Text> ().enabled = true;
			canvas.transform.GetChild(3).GetComponent<Text>().text = "Who's the sender : " + theSender.name + ", args received: " + theArgs.SomeIntParameter + ", " + theArgs.SomeStringParameter;
		}
	}

	/// <summary>
	/// Raises the destroy event.
	/// </summary>
	void OnDestroy(){
		GameLib.EventManager.RemoveListener("mainscene.gate-opened", this.OnGateOpened);

	//----------------------------//

	//---------FUNCTIONS-----------//

	}
}
